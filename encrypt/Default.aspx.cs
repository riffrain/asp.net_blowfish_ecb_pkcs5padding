﻿using System;
using System.Web;
using System.Web.UI;
using BlowFishCS;

namespace encrypt
{
	public partial class Default : System.Web.UI.Page
	{
    	protected void Page_Load(object sender, EventArgs e)
  		{
			string strKey = "key";
			string strPlain = "sample";

			// Encode
			string strCipher = encodeBlowfish_ECB(strKey, strPlain, true);
			lblText.Text = strCipher;

			// Decode
			lblText2.Text = decodeBlowfish_ECB(strKey, strCipher, true);
		}

		private string encodeBlowfish_ECB(string strKey, string strPlain, bool usePKCS5)
		{
			string strCipher = "";
			string strInput;

			if (!isHexString(strKey))
			{
				strKey = str2hex(strKey);
			}

			if (usePKCS5)
			{
				strInput = PKCS5Padding(strPlain);
			}
			else
			{
				strInput = strPlain;
			}

			BlowFish b = new BlowFish(strKey);
			strCipher = b.Encrypt_ECB(strInput);

			return strCipher;
		}

		private string decodeBlowfish_ECB(string strKey, string strCipher, bool usePKCS5)
		{
			string strPlain = "";
			string strInput;

			if (!isHexString(strKey))
			{
				strKey = str2hex(strKey);
			}

			if (usePKCS5)
			{
				strInput = PKCS5Unpadding(strCipher);
			}
			else
			{
				strInput = strCipher;
			}

			BlowFish b = new BlowFish(strKey);
			strPlain = b.Decrypt_ECB(strInput);

			return strPlain;
		}

		/// PKCS#5 Padding
		private string PKCS5Padding(string inStr)
		{
			int nPad = 0;
			nPad = 8 - (inStr.Length % 8);
			for (int i = 0; i < nPad; i++)
			{
				inStr += (char)nPad;
			}
			return inStr;
		}

		/// PKCS#5 Unpadding
		private string PKCS5Unpadding(string inStr)
		{
			int nCode = (int)(inStr[inStr.Length - 1]);
			if (nCode > inStr.Length) return inStr;
			if (strspn(inStr, (char)nCode) != nCode) return inStr;

			return inStr.Substring(0, inStr.Length - 1 * nCode);
		}

		public string str2hex(string strInput)
		{
			string strHex = "";
			foreach (char c in strInput)
			{
				strHex += String.Format("{0:X2}", Convert.ToInt32(c));
			}
			return strHex;
		}

		private int strspn(string strInput, char charMask)
		{
			int count = 0;
			char[] charArray = strInput.ToCharArray();
			Array.Reverse(charArray);
			string strRev = new string(charArray);
			foreach (char c in strRev)
			{
				if (charMask != c) break;
				count++;
			}
			return count;
		}

		private static bool isHexString(string hx)
		{
			foreach (char c in hx.ToCharArray())
			{
				if (!Uri.IsHexDigit(c)) return false;
			}
			return true;
		}

	}
}
